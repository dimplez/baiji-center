import { Calendar } from '@fullcalendar/core';
import interactionPlugin from '@fullcalendar/interaction';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';

(function($) 
{
    // TODO: Test in mobile
    const _getCurrentDateByTimeZone = (timezone) =>
    {
        let currentDate = new Date().toLocaleString("en-US", { timeZone: timezone });

        return new Date(currentDate);
    };

    const _timeFormatter = (date) => 
    {
        let hours   = date.getHours(),
            minutes = date.getMinutes(),
            seconds = date.getSeconds();

        if (hours < 10) hours = '0' + hours;
        if (minutes < 10) minutes = '0' + minutes;
        if (seconds < 10) seconds = '0' + seconds;

        return hours + ':' + minutes + ':' + seconds;
    }

    const _onEventDrag = (info, isEdit) => 
    {
        $('#add-schedule').off().modal('show')
        .on('shown.bs.modal', (event) => 
        {
            const modal = $(event.target),
                  title = isEdit ? info.event.title : info.title,
                  start = isEdit ? info.event.start : info.start,
                  end   = isEdit ? info.event.end : info.end;

            $('#title', modal).val(title).trigger('focus');
            $('#start', modal).val(_timeFormatter(start));
            $('#end', modal).val(_timeFormatter(end));
        });
    };

    // Initialize Scheduler Calendar
    const SchedulerCalendar = (info) => 
    {
        const calendarEl = document.getElementById('calendar'),
            options = {
                plugins: [ interactionPlugin, dayGridPlugin, timeGridPlugin, listPlugin ],
                header: { 
                    left: 'dayGridMonth, timeGridWeek, dayGridDay',
                    center: 'title' 
                },
                defaultView: 'timeGridWeek',
                allDayText: 'All Day',
                nowIndicator: true,
                now: _getCurrentDateByTimeZone("Asia/Shanghai"),
                slotEventOverlap: false,
                navLinks: true,
                selectable: true,
                selectMirror: true,
                select: (info) => _onEventDrag(info, false),
                eventSources: [{
                    // events: (info, successCallback, failureCallback) => 
                    // {
                    //     //TODO: Onload events
                    // },
                    events: [
                        {
                            title  : 'event1',
                            start  : '2019-07-08',
                            end    : '2019-07-09'
                        },
                        {
                            title  : 'event2',
                            start  : '2019-07-11T16:30:00',
                            end    : '2019-07-11T20:00:00'
                        },
                        {
                            title  : 'event3',
                            start  : '2019-07-09T02:30:00',
                            end    : '2019-07-09T06:00:00'
                        }
                    ],
                    allDayDefault: false,
                    textColor: 'white',
                    borderColor: 'black'
                }],
                eventClick: (info) =>  _onEventDrag(info, true)
            };

        if(calendarEl != null)
            new Calendar(calendarEl, options).render();
    };

	var init = () => 
    {
        SchedulerCalendar();
    }
	
	init();

})(jQuery);