@extends('layouts.app')

@section('title', __('Apply'))

@push('styles')
@endpush

@section('content')
<div id="apply" class="apply container-fluid h-100 py-5">
    <div class="row align-items-center justify-content-center h-100">
        <div class="col-md-8">
            <div class="card">
                <img src="{{ asset('images/carousel2.jpg') }}" class="card-img-top" alt="banner">

                <div class="card-body">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button> 
                            
                            <strong>{{ $message }}</strong>
                        </div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="/apply" method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="based" class="text-md-right font-weight-bold">{{ __('Type') }} <small class="text-danger">*</small></label>

                            <select name="based" class="custom-select" id="based" required>
                                <option disabled selected value="">{{ __('Choose Type...') }}</option>
                                <option value="Homebased">{{ __('Homebased') }}</option>
                                <option value="Officebased">{{ __('Officebased') }}</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="name" class="font-weight-bold">{{ __('Name') }} <small class="text-danger">*</small></label>
                            <input name="name" type="text" class="form-control" id="name" placeholder="{{ __('Fullname') }}"
                            value="{{ old('name') }}" aria-describedby="name">
                        </div>

                        <div class="form-group">
                            <label for="contact" class="font-weight-bold">{{ __('Contact Number') }} <small class="text-danger">*</small></label>
                            <input name="contact" type="tel" class="form-control" id="contact" placeholder="{{ __('Contact Number') }}"
                            value="{{ old('contact') }}" aria-describedby="contact">
                        </div>

                        <div class="form-group">
                            <label for="email" class="font-weight-bold">{{ __('Email') }} <small class="text-danger">*</small></label>
                            <input name="email" type="email" class="form-control" id="email" placeholder="{{ __('Email') }}"
                            value="{{ old('email') }}" aria-describedby="email">
                        </div>

                        <div class="form-group">
                            <label for="skype" class="font-weight-bold">{{ __('Skype ID') }} <small class="text-danger">*</small></label>
                            <input name="skype" type="text" class="form-control" id="skype" placeholder="{{ __('Skype ID') }}"
                            value="{{ old('skype') }}" aria-describedby="skype">
                        </div>

                        <div class="form-group">
                            <label for="resume" class="font-weight-bold">{{ __('Resume') }} <small class="text-danger">*</small></label>
                            <div class="custom-file">
                                <input name="resume" type="file" class="custom-file-input" id="resume" value="{{ old('resume') }}">
                                <span class="custom-file-label" for="resume">{{ __('Choose file...') }}</span>
                            </div>
                        </div>

                        @csrf

                        <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script type="application/javascript">
        $(function()
        {
            $('.custom-file-input').on('change', function (event)
             {
                $(this).next('.custom-file-label')
                    .html(event.target.files[0].name);
            })
        });
    </script>
@endpush
