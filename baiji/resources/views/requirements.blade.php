@extends('layouts.app')

@section('title', __('Requirements'))

@push('styles')
@endpush

@section('content')
<div id="requirements" class="requirements">
    <div class="banner">
        <img src="{{ asset('images/banner-requirements.jpg') }}" class="img-fluid" alt="Requirements Banner">
    </div>

    <div class="container-fluid py-5">
        @include('shared.nav-pills')

        <div class="row align-items-center justify-content-center">
            <div class="col-md-5">
                <h1 class="display-4 text-center">{{ __('Requirements') }}</h1>

                <div class="card">
                    <div class="card-body">
                        <ul class="list-group list-group-flush">
                          <li class="list-group-item">&#9658; {{ __('College Graduate, College Level, Senior High School Graduate/Under Graduate.') }}</li>
                          <li class="list-group-item">&#9658; {{ __('Must be computer literate.') }}</li>
                          <li class="list-group-item">&#9658; {{ __('Willing to work 8 hrs. and more.') }}</li>
                          <li class="list-group-item">&#9658; {{ __('Able to work Full Time / Part Time.') }}</li>
                          <li class="list-group-item">&#9658; {{ __('A team player and can work with minimum supervision.') }}</li>
                          <li class="list-group-item">&#9658; {{ __('Above 18 years old.') }}</li>
                          <li class="list-group-item text-center"><a href="/apply" class="btn btn-primary btn-lg">{{ __('Apply') }}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
@endpush
