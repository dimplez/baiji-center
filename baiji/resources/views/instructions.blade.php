@extends('layouts.app')

@section('title', __('Instructions'))

@push('styles')
@endpush

@section('content')
<div class="container-fluid h-100 py-5">
    <div class="row align-items-center justify-content-center">
        <div class="col-sm-10 col-xl-7">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">BAIJI ONLINE LANGUAGE CENTER</h5>

                    <p class="card-text">
                        Is currently looking for: <span class="font-weight-bold">ONLINE ENGLISH INSTRUCTORS (OFFICE-BASED/HOMEBASED)</span>
                    </p>

                    <p class="card-text font-weight-bold">QUALIFICATIONS & REQUIREMENTS:</p>

                    <ul class="list-unstyled">
                        <li>- College graduate, College level, Senior high school graduate/Under graduate</li>
                        <li>- A team player and can work with minimum supervision</li>
                        <li>- Must be computer literate</li>
                        <li>- Willing to work 8 hrs. and more </li>
                        <li>- Able to work full time/Part time </li>
                    </ul>

                    <p class="card-text font-weight-bold">RESPONSIBILITIES:</p>

                    <ul class="list-unstyled">
                        <li>- Teach online English programs to students</li>
                        <li>- Conduct evaluation tests to students by following the method provided by the company.</li>
                    </ul>

                    <p class="card-text font-weight-bold">BENEFITS:</p>

                    <ul class="list-unstyled">
                        <li>A. Compensation Package (FULL TIME) - Class Incentives</li>
                        <li>B. Salary Range 105-150PHP per hour</li>
                    </ul>

                    <p class="card-text font-weight-bold">How to Apply:</p>

                    <ul>
                        <li>
                            <p class="card-text">Interested applicants must bring an updated resume to:</p>
                            
                            <address>
                                #608 UNIT L Ming's building. <br/> 
                                Don Juico ave. <br/>
                                BRGY Malabañas Angeles City Pampanga. 
                            </address>
                        </li>
                        <li class="mb-2">
                            <p class="card-text">Send by email attachment through: <a href="mailto:baijionlinelanguagecenter@gmail.com" class="font-weight-bold">baijionlinelanguagecenter@gmail.com</a></p>                            
                        </li>
                        <li>
                            <p class="card-text">for inquiries you may call: <span class="font-weight-bold">+639959137430</span></p>
                        </li>
                    </ul>

                    <p class="card-text font-weight-bold">Or you can simply apply to this <a href="{{ route('apply') }}" class="font-weight-bold">link</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
@endpush
