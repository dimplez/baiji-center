@extends('layouts.app')

@section('title', __('Contact'))

@push('styles')
@endpush

@section('content')
<div id="contact" class="contact">
    <div class="banner">
        <img src="{{ asset('images/contact-us.jpg') }}" class="img-fluid" alt="Contact Banner">
    </div>

    <div class="container-fluid">
        @include('shared.nav-pills')

        <div class="row">
            <div class="col">
                <h1 class="display-4 text-center">{{ __('Contact Us') }}</h1>

                @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                        
                        <strong>{{ $message }}</strong>
                    </div>
                @endif

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="container-fluid pb-5">
                    <div class="row align-items-center justify-content-center h-100">
                        <div class="col-md-8">
                            <div class="card">
                                <div class="card-body">
                                    <form action="/contact" method="POST" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label for="name" class="font-weight-bold">{{ __('Name') }} <small class="text-danger">*</small></label>
                                            <input name="name" type="text" class="form-control" id="name" placeholder="{{ __('Fullname') }}"
                                            value="{{ old('name') }}" aria-describedby="name">
                                        </div>

                                        <div class="form-group">
                                            <label for="email" class="font-weight-bold">{{ __('Email') }} <small class="text-danger">*</small></label>
                                            <input name="email" type="email" class="form-control" id="email" placeholder="{{ __('Email') }}"
                                            value="{{ old('email') }}" aria-describedby="email">
                                        </div>

                                        <div class="form-group">
                                            <label for="contact" class="font-weight-bold">{{ __('Contact Number') }}</label>
                                            <input name="contact" type="tel" class="form-control" id="contact" placeholder="{{ __('Contact Number') }}"
                                            value="{{ old('contact') }}" aria-describedby="contact">
                                        </div>

                                        <div class="form-group">
                                            <label for="message" class="font-weight-bold">{{ __('Message') }}</label>

                                            <textarea name="message" class="form-control" id="message" rows="3" placeholder="{{ __('Message') }}"></textarea>
                                        </div>
                                        
                                        @csrf

                                        <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
@endpush
