<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Baiji - @yield('title')</title>

    <!-- Styles -->
    <link type="text/css" rel="stylesheet" href="{{ mix('css/app.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ mix('css/styles.css') }}">

    @stack('styles')
</head>
<body>
    <a class="skippy sr-only sr-only-focusable" href="#content">
        <span class="skippy-text">Skip to main content</span>
    </a>

    <div id="app" class="dashboard-layout d-flex flex-column h-100">
        <ul class="navbar-nav ml-auto mr-4">
            @php $locale = session()->get('locale'); @endphp
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    @switch($locale)
                        @case('jp')
                            <img src="{{ asset('images/icon-japanFlag.png') }}" width="30px" height="20x"> Japanese
                        @break

                        @case('vi')
                            <img src="{{ asset('images/icon-vietnameseFlag.png') }}" width="30px" height="20x"> Vietnamese
                        @break
                        
                        @default
                            <img src="{{ asset('images/icon-usaFlag.png') }}" width="30px" height="20x"> English
                    @endswitch <span class="caret"></span>
                </a>
                
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="lang/en"><img src="{{ asset('images/icon-usaFlag.png') }}" width="30px" height="20x"> English</a>
                    <a class="dropdown-item" href="lang/jp"><img src="{{ asset('images/icon-japanFlag.png') }}" width="30px" height="20x"> Japanese</a>
                    <a class="dropdown-item" href="lang/vi"><img src="{{ asset('images/icon-vietnameseFlag.png') }}" width="30px" height="20x"> Vietnamese</a>
                </div>
            </li>
        </ul>

        <header role="banner" class="navbar navbar-expand navbar-light bg-white shadow-sm flex-column flex-sm-row bd-navbar">
            <div>
                <button class="btn btn-light btn-lg d-md-none" type="button" data-toggle="collapse" data-target="#sidebar-items" aria-controls="sidebar-items" aria-expanded="true" aria-label="Toggle docs navigation">
                    <i class="fas fa-bars"></i>
                </button>

                <a class="navbar-brand mr-sm-2" href="/{{ Request::segment(1) }}">
                    <img src="{{ asset('images/logo.jpg') }}" alt="Baiji Logo">
                </a>
            </div>

            @include('shared.navlinks')
        </header>

        <div class="cont row no-gutters flex-grow-1">
            <aside class="sidebar col-12 col-md-2">
                <ul id="sidebar-items" class="collapse d-md-block list-group list-group-flush">
                    @section('sidebar')
                        <li class="list-group-item list-group-item-action">
                            <a href="/{{ Request::segment(1) }}" class="btn btn-link btn-block text-left"><i class="fas fa-calendar-alt"></i> {{ __('Schedule') }}</a>
                        </li>
                    @show
                </ul>
            </aside>

            <main role="main" id="main" class="main col-12 col-md-10 p-3">
                @yield('content')
            </main>
        </div>

        <footer role="contentinfo">
            <small class="text-center d-block">
                &copy; 2019 Baiji Center <br />
                Version: 0.0.1
            </small>

            @include('shared.modals')
        </footer>

    </div>

    <script src="{{ mix('js/app.js') }}"></script>

    @stack('scripts')
</body>
</html>
