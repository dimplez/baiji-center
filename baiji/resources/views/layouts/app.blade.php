<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Baiji - @yield('title')</title>

    <!-- Styles -->
    <link type="text/css" rel="stylesheet" href="{{ mix('css/app.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ mix('css/styles.css') }}">

    @stack('styles')
</head>
<body>
    <a class="skippy sr-only sr-only-focusable" href="#main">
        <span class="skippy-text">Skip to main content</span>
    </a>

    <div id="app" class="home-layout d-flex flex-column h-100">
        <ul class="navbar-nav ml-auto mr-4 localization">
            @php $locale = session()->get('locale'); @endphp
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    @switch($locale)
                        @case('jp')
                            <img src="{{ asset('images/icon-japanFlag.png') }}" width="30px" height="20x"> Japanese
                        @break

                        @case('vi')
                            <img src="{{ asset('images/icon-vietnameseFlag.png') }}" width="30px" height="20x"> Vietnamese
                        @break
                        
                        @default
                            <img src="{{ asset('images/icon-usaFlag.png') }}" width="30px" height="20x"> English
                    @endswitch <span class="caret"></span>
                </a>
                
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="lang/en"><img src="{{ asset('images/icon-usaFlag.png') }}" width="30px" height="20x"> English</a>
                    <a class="dropdown-item" href="lang/jp"><img src="{{ asset('images/icon-japanFlag.png') }}" width="30px" height="20x"> Japanese</a>
                    <a class="dropdown-item" href="lang/vi"><img src="{{ asset('images/icon-vietnameseFlag.png') }}" width="30px" height="20x"> Vietnamese</a>
                </div>
            </li>
        </ul>

        <header role="banner" class="navbar navbar-expand navbar-light bg-white shadow-sm flex-column flex-md-row bd-navbar">
            <a class="navbar-brand mr-sm-2" href="@guest {{ url('/') }} @else {{ url('/home') }} @endguest">
                <img src="{{asset('images/logo.jpg')}}" alt="Baiji Logo">
            </a>

            @include('shared.navlinks')
        </header>

        <main role="main" id="main" class="flex-grow-1">
            @yield('content')
        </main>

        <footer role="contentinfo">
            <div class="container-fluid">
                <div class="row">
                    <div class="col text-center">
                        <a href="/">
                            <img src="{{ asset('images/logo-footer.jpg') }}" class="logo img-fluid" alt="Baiji Logo">
                        </a>
                    </div>
                    <div class="col">
                        <h5 class="title"><i class="fas fa-envelope"></i> {{ __('CONTACT US') }}</h5>

                        <address>
                            <strong>{{ __('BAIJI Online Language Center') }}</strong><br>
                            {{ __('#608 UNIT L Ming\'s building.') }}<br>
                            {{ __('Don Juico ave. Brgy. Malabañas') }}<br>
                            {{ __('Angeles City Pampanga.') }}<br/>
                            <abbr title="Phone">P:</abbr> <strong>+639959137430</strong>
                        </address>
                    </div>
                    <!--<div class="col">
                        <h5 class="title"><i class="fas fa-bullseye"></i> {{ __('OUR MISSION') }}</h5>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                        Praesent tempor.</p>

                        <h5 class="title"><i class="fas fa-comments"></i> {{ __('OUR PURPOSE') }}</h5>

                        <p class="mb-0">Duis ultrices dolor nisl, ac ultricies nisi suscipit vel. 
                        Nullam vulputate feugiat pharetra.</p>
                    </div>-->
                    <div class="col">
                        <h5 class="title"><i class="fas fa-share-alt"></i> {{ __('CONNECT') }}</h5>

                        <div class="social d-flex flex-column align-items-start">
                            <span class="badge badge-light">
                                <a href="#"><i class="fab fa-facebook-square"></i> {{ __('Facebook') }}</a>
                            </span>
                            <span class="badge badge-light">
                                <a href="#"><i class="fab fa-linkedin"></i> {{ __('LinkedIn') }}</a>
                            </span>
                            <span class="badge badge-light">
                                <a href="mailto:baijionlinelanguagecenter@gmail.com"><i class="fas fa-inbox"></i> {{ __('Email') }}</a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <script src="{{ mix('js/app.js') }}"></script>

    @stack('scripts')
</body>
</html>
