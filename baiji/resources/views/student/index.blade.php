@extends('layouts.dashboard')

@section('title', __('Employee'))

@push('styles')
@endpush

@section('sidebar')
    @parent

    @include('shared.student-sidebar')
@endsection

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">{{ __('Student') }}</li>
            <li class="breadcrumb-item active" aria-current="page"><span>{{ __('Schedule') }}</span></li>
        </ol>
    </nav>

    <div id="calendar"></div>
@endsection

@push('scripts')
@endpush
