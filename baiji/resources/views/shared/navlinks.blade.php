<div class="navbar-nav-scroll ml-md-auto">
    <ul class="navbar-nav bd-navbar-nav flex-row">
        <!-- Authentication Links -->
        @guest
            <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
            <!--<li class="nav-item">
                <a class="nav-link" href="{{ route('about') }}">{{ __('About Us') }}</a>
            </li>-->
            <li class="nav-item">
                <a class="nav-link" href="{{ route('requirements') }}">{{ __('Requirements') }}</a>
            </li>
            <!--<li class="nav-item">
                <a class="nav-link" href="{{ route('careers') }}">{{ __('Careers') }}</a>
            </li>-->
            <li class="nav-item">
                <a class="nav-link" href="{{ route('map') }}"> {{ __('Map') }}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('contact') }}">{{ __('Contact Us') }}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('apply') }}">{{ __('Apply') }}</a>
            </li>
        @else
            <li class="nav-item">
                <a class="nav-link" href="{{ route('apply') }}">{{ __('Apply') }}</a>
            </li>
            @if (Auth::user()->is_admin)
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
            @endif
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ __(Auth::user()->name) }} <span class="caret"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">
                        <i class="fas fa-user-edit"></i>
                        {{ __('Edit Profile') }}
                    </a>
                    <a class="dropdown-item" href="#">
                        <i class="fas fa-cog"></i>
                        {{ __('Settings') }}
                    </a>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </div>
            </li>
        @endguest
    </ul>
</div>