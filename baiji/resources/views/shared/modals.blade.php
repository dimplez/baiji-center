<div id="add-schedule" class="add-schedule modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ __('Add Schedule') }}</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="POST">
                    <div class="form-group">
                        <label for="title">{{ __('Title') }}</label>
                        <input type="text" class="form-control" id="title" placeholder="{{ __('Title') }}">
                    </div>
                    <div class="form-row">
                        <div class="col">
                            <div class="form-group">
                                <label for="start">{{ __('Start') }}</label>
                                <input type="time" class="form-control" id="start" placeholder="{{ __('Start') }}">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="end">{{ __('End') }}</label>
                                <input type="time" class="form-control" id="end" placeholder="{{ __('End') }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="title">{{ __('Client') }}</label>
                        <select class="client custom-select" required>
                            <option disabled selected value="">{{ __('Select client...') }}</option>
                            <option value="0">John Doe</option>
                            <option value="1">Xander Form</option>
                            <option value="2">Jake Xyruz</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="notes">{{ __('Notes') }}</label>
                        <textarea class="form-control" id="notes" rows="3"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
                <button type="button" class="btn btn-primary">{{ __('Save') }}</button>
            </div>
        </div>
    </div>
</div>