<li class="list-group-item list-group-item-action">
    <div class="btn-group dropright">
        <button type="button" class="btn btn-link btn-block dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-user-graduate"></i> {{ __('Teachers') }}
        </button>

        <div class="dropdown-menu">
            <a class="dropdown-item" href="/admin/teachers/add"><i class="fas fa-plus"></i> {{ __('Add') }}</a>
        </div>
    </div>
</li>