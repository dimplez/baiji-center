@extends('layouts.app')

@section('title', __('Verify'))

@push('styles')
@endpush

@section('content')
<div class="container-fluid h-100">
    <div class="row align-items-center justify-content-center h-100">
        <div class="col-8 col-md-6 col-lg-4">
            <div class="card my-5">
                <div class="card-header">{{ __('Verify Your Email Address') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
@endpush