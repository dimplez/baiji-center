@extends('layouts.dashboard')

@section('title', __('Create'))

@push('styles')
@endpush

@section('sidebar')
    @parent

    @include('shared.admin-sidebar')
@endsection

@section('content')
    <form action="/admin/teachers/create" method="POST" enctype="multipart/form-data">
        @method('PUT')

        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="fullname" class="font-weight-bold">{{ __('Full Name') }} <small class="text-danger">*</small></label>
                    <input type="text" name="fullname" id="fullname" class="form-control" value="{{ old('fullname') }}"
                     placeholder="{{ __('Full name') }}" aria-describedby="fullname">
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="photo" class="font-weight-bold">{{ __('Photo') }}</label>
                    <div class="custom-file">
                        <input name="photo" type="file" class="custom-file-input" id="photo" value="{{ old('photo') }}">
                        <span class="custom-file-label" for="photo">{{ __('Choose file...') }}</span>
                    </div>
                </div>
            </div>
        </div>
        
         <div class="form-group">
            <label for="description" class="font-weight-bold">{{ __('Self Introduction') }} <small class="text-danger">*</small></label>

            <textarea name="description" class="form-control" id="description" rows="3" placeholder="{{ __('Self Introduction') }}"></textarea>
        </div>

        <div class="form-group">
            <label for="video" class="font-weight-bold">{{ __('Video') }} <small class="text-danger">*</small></label>
            <div class="custom-file">
                <input name="video" type="file" class="custom-file-input" id="video" value="{{ old('video') }}">
                <span class="custom-file-label" for="video">{{ __('Choose file...') }}</span>
            </div>
        </div>

        @csrf
    <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
@endsection

@section('scripts')
@endsection