@extends('layouts.dashboard')

@section('title', __('Admin'))

@push('styles')
@endpush

@section('sidebar')
    @parent
    
    @include('shared.admin-sidebar')
@endsection

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">{{ __('Admin') }}</li>
            <li class="breadcrumb-item active" aria-current="page"><span>{{ __('Schedule') }}</span></li>
        </ol>
    </nav>

    <div id="calendar"></div>
@endsection

@section('scripts')
@endsection