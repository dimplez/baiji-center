@extends('layouts.app')

@section('title', __('Map'))

@push('styles')
@endpush

@section('content')
<div id="map" class="map">
    <div class="banner">
        <img src="{{ asset('images/map-banner.jpg') }}" class="img-fluid" alt="Map Banner">
    </div>

    <div class="container-fluid">
        @include('shared.nav-pills')

        <div class="row">
            <div class="col">
                <h1 class="display-4 text-center">{{ __('Visit us:') }}</h1>

                <a href="{{ asset('images/map.jpg') }}" target="_blank">
                    <img src="{{ asset('images/map.jpg') }}" class="map-img img-fluid" alt="Map">
                </a>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
@endpush
