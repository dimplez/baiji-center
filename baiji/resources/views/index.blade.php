@extends('layouts.app')

@section('title',  __('Online Language Center'))

@push('styles')
@endpush

@section('content')
    <div id="index" class="index">
        <div id="indexCarousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#indexCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#indexCarousel" data-slide-to="1" class=""></li>
                <li data-target="#indexCarousel" data-slide-to="2" class=""></li>
            </ol>

            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="{{ asset('images/carousel1.jpg') }}" class="d-block w-100" alt="Carousel 1" />
                </div>
                <div class="carousel-item">
                    <img src="{{ asset('images/carousel2.jpg') }}" class="d-block w-100" alt="Carousel 2" />
                </div>
                <div class="carousel-item">
                    <img src="{{ asset('images/carousel3.jpg') }}" class="d-block w-100" alt="Carousel 3" />
                </div>
            </div>

            <a class="carousel-control-prev" href="#indexCarousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#indexCarousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

        <div class="container-fluid">
            @include('shared.nav-pills')

            <div class="showcase card-deck">
                <div class="card">
                    <svg class="img-thumbnail img-fluid" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Image of the Office">
                        <title>Image of the office</title>
                        <rect width="100%" height="100%" fill="#868e96"></rect>
                        <text x="50%" y="50%" fill="#dee2e6" dominant-baseline="central" text-anchor="middle">Image</text>
                    </svg>
                </div>
                <div class="card">
                    <svg class="img-thumbnail img-fluid" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Image of the Office">
                        <title>Image of the office</title>
                        <rect width="100%" height="100%" fill="#868e96"></rect>
                        <text x="50%" y="50%" fill="#dee2e6" dominant-baseline="central" text-anchor="middle">Image</text>
                    </svg>
                </div>
                <div class="card">
                    <svg class="img-thumbnail img-fluid" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Image of the Office">
                        <title>Image of the office</title>
                        <rect width="100%" height="100%" fill="#868e96"></rect>
                        <text x="50%" y="50%" fill="#dee2e6" dominant-baseline="central" text-anchor="middle">Image</text>
                    </svg>
                </div>
            </div>

            <hr class="my-5">

            <div class="marketing container">
                <div class="row">
                    <div class="col-md-7">
                        <h2>{{ __('Baiji Online Language Center') }}</h2>

                        <p class="lead">
                            {{ __('We offer an alternative way of learning the English language through private virtual learning environment.') }}
                        </p>
                    </div>
                    <div class="col-md-5">
                        <svg class="img-thumbnail img-fluid" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Image">
                            <title>Placeholder</title>
                            <rect width="100%" height="100%" fill="#eee"></rect>
                            <text x="50%" y="50%" fill="#aaa" dominant-baseline="central" text-anchor="middle">Image</text>
                        </svg>
                    </div>
                </div>

                <hr class="my-5">

                <div class="row">
                    <div class="col-md-5 order-md-1">
                        <svg class="img-thumbnail img-fluid" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Image">
                            <title>Placeholder</title>
                            <rect width="100%" height="100%" fill="#eee"></rect>
                            <text x="50%" y="50%" fill="#aaa" dominant-baseline="central" text-anchor="middle">Image</text>
                        </svg>
                    </div>
                    <div class="col-md-7 order-md-2">
                        <h2>{{ __('What makes us different?') }}</h2>

                        <p class="lead">{{ __('Our highly qualified teachers provide personalized approach in tutorial sessions based on student\'s needs and competencies.') }}</p>
                    </div>
                </div>

                <hr class="my-5">

                <div class="row">
                    <div class="col-md-7">
                        <h2>{{ __('Why choose us?') }}</h2>

                        <p class="lead">
                            {{ __('We offer our partner-teachers flexible working schedules and exciting perks at par with the best practices in the industry. On top of that, we give referral bonus for every successful teacher applicant and sign-up bonus for new student enrollee.') }}
                        </p>
                    </div>
                    <div class="col-md-5">
                        <svg class="img-thumbnail img-fluid" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Image">
                            <title>Placeholder</title>
                            <rect width="100%" height="100%" fill="#eee"></rect>
                            <text x="50%" y="50%" fill="#aaa" dominant-baseline="central" text-anchor="middle">Image</text>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
@endpush