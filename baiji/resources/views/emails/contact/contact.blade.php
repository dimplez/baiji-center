@component('mail::panel')
# Contact Form

<p><strong>Name: </strong>  {{ $data['name'] }}</p>
<p><strong>Email: </strong> {{ $data['email'] }}<p>
<p><strong>Contact Number: </strong> {{ $data['contact'] }}<p>
<p><strong>Message: </strong> {{ $data['message'] }}<p>

@endcomponent
