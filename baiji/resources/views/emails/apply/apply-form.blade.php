@component('mail::panel')
# New Application

<p><strong>Type: </strong>  {{ $data['based'] }}</p>
<p><strong>Name: </strong>  {{ $data['name'] }}</p>
<p><strong>Contact Number: </strong> {{ $data['contact'] }}<p>
<p><strong>Email: </strong> {{ $data['email'] }}<p>
<p><strong>Skype ID: </strong>  {{ $data['skype'] }}</p>

@endcomponent
