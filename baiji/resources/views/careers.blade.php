@extends('layouts.app')

@section('title', __('Careers'))

@push('styles')
@endpush

@section('content')
<div id="careers" class="careers">
    <div class="banner">
        <img src="{{ asset('images/banner-careers.jpg') }}" class="img-fluid" alt="Careers Banner">
    </div>

    <div class="container-fluid">
        @include('shared.nav-pills')

        <div class="row">
            <div class="col">
                <h1 class="display-4 text-center">{{ __('Careers') }}</h1>

                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sit amet vulputate lacus. 
                    Donec eget consectetur massa, eget facilisis ligula. Sed pellentesque sapien sed ipsum 
                    dapibus auctor sed sit amet magna. Aliquam a enim in nisi accumsan ultrices. Suspendisse aliquet 
                    sagittis elit. Maecenas sed pulvinar leo. Nullam nunc enim, ultricies non egestas fringilla, luctus 
                    sit amet nibh.
                </p>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
@endpush
