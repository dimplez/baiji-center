<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Mail\ContactForm;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function create()
    {
        return view('contact');
    }

    public function store(Request $request)
    {
        $data = request()->validate([
            'name' => 'required',
            'email' => 'required|email',
            'contact' => 'nullable',
            'message' => 'nullable|max:255'
        ]);

        Mail::to('baijionlinelanguagecenter@gmail.com')->send(new ContactForm($data));

        return back()->with('success', 'Thanks for contacting us. We\'ll get back to you ASAP!');
    }
}
