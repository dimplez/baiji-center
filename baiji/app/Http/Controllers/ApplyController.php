<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Mail\ApplyForm;
use Illuminate\Support\Facades\Mail;


class ApplyController extends Controller
{
    public function create()
    {
        return view('apply');
    }

    public function store(Request $request)
    {
        $data = request()->validate([
            'based' => 'required',
            'name' => 'required',
            'contact' => 'required',
            'email' => 'required|email',
            'skype' => 'required',
            'resume' => 'required|max:10000|mimes:doc,docx,pdf' 
        ]);

        Mail::to('baijionlinelanguagecenter@gmail.com')->send(new ApplyForm($data));

        return back()->with('success', 'Application Submitted!');
    }
}
