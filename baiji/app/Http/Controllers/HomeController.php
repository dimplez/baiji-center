<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        switch (auth()->user()->is_admin) {
            case 0:
                return redirect()->action('Student\StudentController@index');
                break;

            case 1:
                return redirect()->action('Admin\AdminController@index');
                break;
                
            default:
                return view('/');
                break;
        }
    }
}
