<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApplyForm extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->markdown('emails.apply.apply-form')
             ->subject('Job Application')
             ->attach($this->data['resume']->getRealPath(),
             [
                'as' => $this->data['resume']->getClientOriginalName(),
                'mime' => $this->data['resume']->getClientMimeType(),
             ]);
    }
}
