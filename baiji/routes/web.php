<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () 
{
    return view('index');
});

Auth::routes();

Route::get('/lang/{locale}', function($locale)
{
    App::setLocale($locale);

    session()->put('locale', $locale);

    return redirect()->back();
});

Route::get('/home', 'HomeController@index')->name('home');

Route::view('/about', 'about')->name('about');
Route::view('/requirements', 'requirements')->name('requirements');
Route::view('/careers', 'careers')->name('careers');
Route::view('/map', 'map')->name('map');

Route::get('/contact', 'ContactController@create')->name('contact');
Route::post('/contact', 'ContactController@store');

Route::view('/instructions', 'instructions')->name('instructions');

Route::get('/apply', 'ApplyController@create')->name('apply');
Route::post('/apply', 'ApplyController@store');

Route::group(['prefix' => 'admin'], function()
{
    Route::get('/', 'Admin\AdminController@index')->name('admin');

    Route::get('/teachers/add', 'Admin\TeachersController@index')->name('teachers-create');
    Route::post('/teachers/add', 'Admin\TeachersController@store');
});


Route::get('/student', 'Student\StudentController@index')->name('student');
