const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.setPublicPath('../public_html/');
mix.js([
        'resources/js/app.js',
        'resources/js/scripts.js'
    ], '../public_html/js/app.js')
   .sass('resources/sass/app.scss', '../public_html/css/')
   .sass('resources/sass/styles.scss', '../public_html/css')
   .disableNotifications();

if(process.env.NODE_ENV == 'production') 
{
    mix.version();
}